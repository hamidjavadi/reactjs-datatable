import { Route, BrowserRouter } from "react-router-dom";
import DataTable from "../components/data-table/data-table";
import FilterForm from "../components/filter-form/filter-form";
import React, { Component } from "react";
import SortToolbar from "../components/sort-toolbar/sort-toolbar";
import "./App.css";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <Route path="/" exact component={FilterForm} />
                    <Route path="/" exact component={SortToolbar} />
                    <Route path="/" exact component={DataTable} />
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
