export const LoadWarehouseData = "LoadWarehouseData";
export const SetFilteredData = "SetFilteredData";
export const SetCurrentPage = "SetCurrentPage";
export const SortData = "SortData";
