import * as DataManagementActions from "./data-management-actions";
import * as LoadingActions from "./data-management-actions";
import * as FilterActions from "./filter-actions";

const actions = {
    DataManagementActions,
    LoadingActions,
    FilterActions,
};

export default actions;
