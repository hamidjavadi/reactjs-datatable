import * as Actions from "../actions/loading-actions";

const initialState = {
    isLoading: false,
};

function loadingReducer(state = initialState, action) {
    switch (action.type) {
        case Actions.EndLoading:
            break;
        case Actions.StartLoading:
            return { isLoading: true };
        default:
            break;
    }
    return state;
}

export default loadingReducer;
