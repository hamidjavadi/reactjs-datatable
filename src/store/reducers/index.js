import { combineReducers } from "redux";
import DataManagementReducer from "./data-management-reducer";
import LoadingReducer from "./loading-reducer";

const StoreReducers = combineReducers({
    DataManagementReducer: DataManagementReducer,
    LoadingReducer: LoadingReducer,
});

export default StoreReducers;
