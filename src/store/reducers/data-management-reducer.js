import * as Actions from "../actions/data-management-actions";

const initialState = {
    currentPage: 1,
    sort: {
        field: "", // TODO: Create an object for values [name, date, title, field, old_value, new_value]
        order: "", // TODO: Create an object for values [asc, desc]
    },
    filteredData: [],
    recordsPerPage: 20,
    warehouseData: [],
};

function dataManagementReducer(state = initialState, action) {
    switch (action.type) {
        case Actions.LoadWarehouseData:
            state = {
                ...state,
                warehouseData: action.payload.records,
            };

            let filteredData = DoFilter(state, action.payload.filterParams);

            return {
                ...state,
                filteredData: filteredData,
            };
        case Actions.SetFilteredData:
            return {
                ...state,
                filteredData: DoFilter(state, action.payload),
                currentPage: 1,
            };
        case Actions.SetCurrentPage:
            return {
                ...state,
                currentPage: action.payload.currentPage,
            };
        case Actions.SortData:
            return {
                ...state,
                sort: action.payload.sort,
                filteredData: DoSort(state, action.payload.sort),
            };
        default:
            break;
    }

    return state;
}

/**
 * Filter data from warehouse
 *
 * @param state object
 * @param filterParams object
 */
function DoFilter(state, filterParams) {
    if (state.warehouseData === undefined) {
        return [];
    }

    return state.warehouseData.filter((item) => {
        let nameIsContain = true;
        let dateIsEquals = true;
        let adnameIsContain = true;
        let fieldIsEquals = true;
        let serachItem = { ...item };

        // Begin convert numbers
        serachItem.name = ConvertPersianNumbersToEn(serachItem.name);
        serachItem.title = ConvertPersianNumbersToEn(serachItem.title);

        filterParams.name = ConvertPersianNumbersToEn(filterParams.name);
        filterParams.adname = ConvertPersianNumbersToEn(filterParams.adname);
        // End convert numbers

        // checking name
        if (filterParams.name !== "") {
            if (serachItem.name.indexOf(filterParams.name) < 0) {
                nameIsContain = false;
            }
        }

        // checking date
        if (filterParams.date !== "") {
            if (serachItem.date !== filterParams.date) {
                dateIsEquals = false;
            }
        }

        // checking adname
        if (filterParams.adname !== "") {
            if (serachItem.title.indexOf(filterParams.adname) < 0) {
                adnameIsContain = false;
            }
        }

        // checking adname
        if (filterParams.field !== "") {
            if (serachItem.field !== filterParams.field) {
                fieldIsEquals = false;
            }
        }

        return (
            nameIsContain && dateIsEquals && adnameIsContain && fieldIsEquals
        );
    });
}

/**
 * Sort the filteredData data
 *
 * @param state object
 * @param sortParams object
 */
function DoSort(state, sortParams) {
    if (sortParams.order === "desc") {
        // Begin sort asc
        return state.filteredData.sort((a, b) => {
            switch (sortParams.field) {
                case "name":
                    if (a.name > b.name) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "date":
                    if (a.date > b.date) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "title":
                    if (a.title > b.title) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "field":
                    if (a.field > b.field) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "old_value":
                    if (a.old_value > b.old_value) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "new_value":
                    if (a.new_value > b.new_value) {
                        return -1;
                    } else {
                        return 1;
                    }
                default:
                    return a.id - b.id;
            }
        });
    } else {
        // Begin sort desc
        return state.filteredData.sort((a, b) => {
            switch (sortParams.field) {
                case "name":
                    if (a.name < b.name) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "date":
                    if (a.date < b.date) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "title":
                    if (a.title < b.title) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "field":
                    if (a.field < b.field) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "old_value":
                    if (a.old_value < b.old_value) {
                        return -1;
                    } else {
                        return 1;
                    }
                case "new_value":
                    if (a.new_value < b.new_value) {
                        return -1;
                    } else {
                        return 1;
                    }
                default:
                    return b.id - a.id;
            }
        });
    }
}

/**
 * Convert Persian numbers to English numbers
 *
 * @param string string
 * @returns string
 */
function ConvertPersianNumbersToEn(string) {
    return string
        .replace(/۰/g, 0)
        .replace(/۱/g, 1)
        .replace(/۲/g, 2)
        .replace(/۳/g, 3)
        .replace(/۴/g, 4)
        .replace(/۵/g, 5)
        .replace(/۶/g, 6)
        .replace(/۷/g, 7)
        .replace(/۸/g, 8)
        .replace(/۹/g, 9);
}

export default dataManagementReducer;
