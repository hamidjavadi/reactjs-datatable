import React, { Component } from "react";
import WishListButton from "../wishlist-button/wishlist-button";

class DataRow extends Component {
    state = {
        isWishlisted: true,
    };

    render() {
        return (
            <div className="row">
                {/* User */}
                <div className="col col-sm-6 user">
                    <span className="icon">
                        <i className="fa fa-user"></i>
                    </span>
                    <span className="value">{this.props.record.name}</span>
                </div>

                {/* Date */}
                <div className="col col-sm-6 date">
                    <span className="icon">
                        <i className="fa fa-calendar"></i>
                    </span>
                    <span className="value">{this.props.record.date}</span>
                </div>

                {/* Title */}
                <div className="col adtitle">
                    <span className="value">{this.props.record.title}</span>
                </div>

                {/* Field name */}
                <div className="col field-name">
                    <span className="icon">
                        <i className="fa fa-refresh"></i>
                    </span>
                    <span className="value">{this.props.record.field}</span>
                </div>

                {/* Old value */}
                <div className="col old-value">
                    <span className="value">{this.props.record.old_value}</span>
                </div>

                {/* To Bottom arrow */}
                <div className="col to-bottom-arrow">
                    <i className="fa fa-arrow-down"></i>
                </div>

                {/* New Value */}
                <div className="col new-value">
                    <span className="value">{this.props.record.new_value}</span>
                </div>

                {/* Wishlist button */}
                <div className="col bottons">
                    <WishListButton
                        key={this.props.record.id}
                        adId={this.props.record.id}
                    />
                </div>
            </div>
        );
    }
}

export default DataRow;
