import React, { Component } from "react";
import "./messagebox.css";

class MessageBox extends Component {
    render() {
        return <div {...this.props.attrs}>{this.props.message}</div>;
    }
}

export default MessageBox;
