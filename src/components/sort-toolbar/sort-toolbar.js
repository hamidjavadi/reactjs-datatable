import Actions from "../../store/actions/index";
import React, { Component } from "react";
import { connect } from "react-redux";
import Store from "../../store/store";
import "./sort-toolbar.css";

class SortToolbar extends Component {
    onSortFieldChanged(event) {
        let field = event.target.value,
            order = this.props.sort.order;

        if (field === this.props.sort.field) {
            return false;
        }

        if (order === "") {
            order = "asc";
        }

        Store.dispatch({
            type: Actions.DataManagementActions.SortData,
            payload: {
                sort: {
                    field: field,
                    order: order,
                },
            },
        });
    }

    onSortClick(event) {
        let order = "asc";

        if (this.props.sort.field === "") {
            return false;
        }

        if (this.props.sort.order === "asc") {
            order = "desc";
        } else {
            order = "asc";
        }

        Store.dispatch({
            type: Actions.DataManagementActions.SortData,
            payload: {
                sort: {
                    field: this.props.sort.field,
                    order: order,
                },
            },
        });
        // console.log(field);
    }

    // TODO: Add items in an array then create by map
    render() {
        let nameProps = {};
        let dateProps = {};
        let titleProps = {};
        let fieldProps = {};
        let old_valueProps = {};
        let new_valueProps = {};

        if (this.props.sort.field === "name") nameProps.selected = true;

        if (this.props.sort.field === "date") dateProps.selected = true;

        if (this.props.sort.field === "title") titleProps.selected = true;

        if (this.props.sort.field === "field") fieldProps.selected = true;

        if (this.props.sort.field === "old_value")
            old_valueProps.selected = true;

        if (this.props.sort.field === "new_value")
            new_valueProps.selected = true;

        return (
            <div className="sort-toolbar">
                <select onChange={(event) => this.onSortFieldChanged(event)}>
                    <option value="">انتخاب مرتب سازی</option>
                    <option value="name" {...nameProps}>
                        نام تغییر دهنده
                    </option>
                    <option value="date" {...dateProps}>
                        تاریخ
                    </option>
                    <option value="title" {...titleProps}>
                        نام آگهی
                    </option>
                    <option value="field" {...fieldProps}>
                        فیلد
                    </option>
                    <option value="old_value" {...old_valueProps}>
                        مقدار قدیمی
                    </option>
                    <option value="new_value" {...new_valueProps}>
                        مقدار جدید
                    </option>
                </select>
                <button
                    className={this.props.sort.order === "" ? "hidden" : ""}
                    onClick={(event) => this.onSortClick(event)}
                >
                    <i
                        className={
                            this.props.sort.order === "asc"
                                ? "fa fa-sort-amount-asc"
                                : "fa fa-sort-amount-desc"
                        }
                    ></i>
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        sort: state.DataManagementReducer.sort,
    };
};

export default connect(mapStateToProps)(SortToolbar);
