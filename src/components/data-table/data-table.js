import React, { Component } from "react";
import { connect } from "react-redux";
import DataRow from "../data-row/datarow";
import MessageBox from "../messagebox/messagebox";
import Store from "../../store/store";
import Actions from "../../store/actions/index";
import "./data-table.css";

class DataTable extends Component {
    loading = false;
    tableRecords = [];

    componentDidMount = () => {
        window.addEventListener("scroll", this.scrollHandler);
    };

    componentWillUnmount = () => {
        window.removeEventListener("scroll", this.scrollHandler);
    };

    sortData(field) {
        let order = "asc";

        if (field === this.props.sort.field) {
            if (this.props.sort.order === "asc") {
                order = "desc";
            } else {
                order = "asc";
            }
        }

        Store.dispatch({
            type: Actions.DataManagementActions.SortData,
            payload: {
                sort: {
                    field: field,
                    order: order,
                },
            },
        });
        // console.log(field);
    }

    getTableRecords = () => {
        let count = this.props.currentPage * this.props.recordsPerPage;
        return this.props.records.slice(0, count);
    };

    scrollHandler = () => {
        let windowHeight = window.innerHeight,
            html = document.getElementsByTagName("html")[0],
            scrollHeight = html.scrollHeight,
            scrollTop = html.scrollTop,
            totalPages = Math.ceil(
                this.props.records.length / this.props.recordsPerPage
            ),
            offset = 100;

        if (scrollTop + windowHeight + offset >= scrollHeight) {
            if (this.tableRecords.length !== this.props.records.length) {
                if (totalPages > this.props.currentPage) {
                    Store.dispatch({
                        type: Actions.DataManagementActions.SetCurrentPage,
                        payload: {
                            currentPage: this.props.currentPage + 1,
                        },
                    });
                }
            }
        }
    };

    render() {
        this.tableRecords = this.getTableRecords();

        if (this.tableRecords.length === 0) {
            return (
                <MessageBox
                    message="رکوردی یافت نشد"
                    attrs={{ className: "message danger" }}
                />
            );
        } else {
            let rows = this.tableRecords.map((record, index) => {
                return <DataRow key={index} record={record} />;
            });

            return (
                <div className="table">
                    <div className="header">
                        <div className="col">
                            <div className="header-title">
                                <span
                                    className="title"
                                    onClick={() => this.sortData("name")}
                                >
                                    نام تغییر دهنده
                                </span>
                                <span
                                    className={
                                        this.props.sort.field === "name"
                                            ? "sort-icon active"
                                            : "sort-icon"
                                    }
                                >
                                    <i
                                        className={
                                            this.props.sort.order === "asc"
                                                ? "fa fa-sort-amount-asc"
                                                : "fa fa-sort-amount-desc"
                                        }
                                    ></i>
                                </span>
                            </div>
                        </div>
                        <div className="col">
                            <div className="header-title">
                                <span
                                    className="title"
                                    onClick={() => this.sortData("date")}
                                >
                                    تاریخ
                                </span>
                                <span
                                    className={
                                        this.props.sort.field === "date"
                                            ? "sort-icon active"
                                            : "sort-icon"
                                    }
                                >
                                    <i
                                        className={
                                            this.props.sort.order === "asc"
                                                ? "fa fa-sort-amount-asc"
                                                : "fa fa-sort-amount-desc"
                                        }
                                    ></i>
                                </span>
                            </div>
                        </div>
                        <div className="col">
                            <div className="header-title">
                                <span
                                    className="title"
                                    onClick={() => this.sortData("title")}
                                >
                                    نام آگهی
                                </span>
                                <span
                                    className={
                                        this.props.sort.field === "title"
                                            ? "sort-icon active"
                                            : "sort-icon"
                                    }
                                >
                                    <i
                                        className={
                                            this.props.sort.order === "asc"
                                                ? "fa fa-sort-amount-asc"
                                                : "fa fa-sort-amount-desc"
                                        }
                                    ></i>
                                </span>
                            </div>
                        </div>
                        <div className="col">
                            <div className="header-title">
                                <span
                                    className="title"
                                    onClick={() => this.sortData("field")}
                                >
                                    فیلد
                                </span>
                                <span
                                    className={
                                        this.props.sort.field === "field"
                                            ? "sort-icon active"
                                            : "sort-icon"
                                    }
                                >
                                    <i
                                        className={
                                            this.props.sort.order === "asc"
                                                ? "fa fa-sort-amount-asc"
                                                : "fa fa-sort-amount-desc"
                                        }
                                    ></i>
                                </span>
                            </div>
                        </div>
                        <div className="col">
                            <div className="header-title">
                                <span
                                    className="title"
                                    onClick={() => this.sortData("old_value")}
                                >
                                    مقدار قدیمی
                                </span>
                                <span
                                    className={
                                        this.props.sort.field === "old_value"
                                            ? "sort-icon active"
                                            : "sort-icon"
                                    }
                                >
                                    <i
                                        className={
                                            this.props.sort.order === "asc"
                                                ? "fa fa-sort-amount-asc"
                                                : "fa fa-sort-amount-desc"
                                        }
                                    ></i>
                                </span>
                            </div>
                        </div>
                        <div className="col">
                            <div className="header-title">
                                <span
                                    className="title"
                                    onClick={() => this.sortData("new_value")}
                                >
                                    مقدار جدید
                                </span>
                                <span
                                    className={
                                        this.props.sort.field === "new_value"
                                            ? "sort-icon active"
                                            : "sort-icon"
                                    }
                                >
                                    <i
                                        className={
                                            this.props.sort.order === "asc"
                                                ? "fa fa-sort-amount-asc"
                                                : "fa fa-sort-amount-desc"
                                        }
                                    ></i>
                                </span>
                            </div>
                        </div>
                        <div className="col">
                            <span>علاقه مندی</span>
                        </div>
                    </div>
                    <div className="body">{rows}</div>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        records: state.DataManagementReducer.filteredData,
        currentPage: state.DataManagementReducer.currentPage,
        recordsPerPage: state.DataManagementReducer.recordsPerPage,
        sort: state.DataManagementReducer.sort,
    };
};

export default connect(mapStateToProps)(DataTable);
