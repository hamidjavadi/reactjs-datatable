import React, { Component } from "react";
import "./wishlist-button.css";

class WishListButton extends Component {
    state = {
        isWishlisted: false,
    };

    messages = {
        add: "افزودن به علاقه مندی",
        remove: "حذف از علاقه مندی",
    };

    onToggleWishList() {
        let itemIndex = -1;
        let isWishlisted = false;
        let localStorageWishlist = localStorage.getItem("wishlist");

        if (localStorageWishlist !== null) {
            localStorageWishlist = JSON.parse(localStorageWishlist);
        } else {
            localStorageWishlist = [];
        }

        /* Begin add or remove this item to wishlist */
        itemIndex = localStorageWishlist.indexOf(this.props.adId);
        if (itemIndex !== -1) {
            localStorageWishlist.splice(itemIndex, 1);
        } else {
            localStorageWishlist.push(this.props.adId);
            isWishlisted = true;
        }
        /* End add or remove this item to wishlist */

        localStorageWishlist = JSON.stringify(localStorageWishlist);
        localStorage.setItem("wishlist", localStorageWishlist);

        this.setState({
            isWishlisted: isWishlisted,
        });
    }

    initialWishlistLocalStorage() {
        let initialLocalStorage = [];
        let localStorageWishlist = localStorage.getItem("wishlist");

        /* Begin init localstorage if not exist */
        if (localStorageWishlist === null) {
            initialLocalStorage = JSON.stringify(initialLocalStorage);
            localStorage.setItem("wishlist", initialLocalStorage);
        }
        /* End init localstorage if not exist */
    }

    setWishStatus() {
        try {
            let itemIndex = -1;
            let isWishlisted = false;
            let localStorageWishlist = localStorage.getItem("wishlist");

            if (localStorageWishlist === null) {
                this.initialWishlistLocalStorage();
                return false;
            } else {
                localStorageWishlist = JSON.parse(localStorageWishlist);
            }

            /* Begin check is wishlisted? */
            itemIndex = localStorageWishlist.indexOf(this.props.adId);

            if (itemIndex !== -1) {
                isWishlisted = true;
            }
            /* End check is wishlisted? */

            // Begin update state
            this.setState({
                isWishlisted: isWishlisted,
            });
        } catch (exception) {
            console.log(exception);
        }
    }

    componentDidMount() {
        this.setWishStatus();
    }

    render() {
        return (
            <div
                className="wishlist-button"
                data-wishlisted={this.state.isWishlisted}
            >
                {/* Begin display star icon only */}
                <span className="show-icon-only">
                    <i
                        className="fa fa-star"
                        title={
                            this.state.isWishlisted === true
                                ? this.messages.remove
                                : this.messages.add
                        }
                        onClick={() => this.onToggleWishList()}
                    ></i>
                </span>
                {/* End display star icon only */}
                <button
                    onClick={() => this.onToggleWishList()}
                    className={
                        this.state.isWishlisted === true
                            ? "btn btn-danger"
                            : "btn btn-success"
                    }
                >
                    <i className="fa fa-star"></i>
                    <span>
                        {this.state.isWishlisted === true
                            ? this.messages.remove
                            : this.messages.add}
                    </span>
                </button>
            </div>
        );
    }
}

export default WishListButton;
