import Data from "../../database/data.json";
import { connect } from "react-redux";
import React, { Component, createRef } from "react";
import Actions from "../../store/actions/index";
import Store from "../../store/store";
import "./filter-form.css";

class FilterForm extends Component {
    inputName = createRef();
    inputDate = createRef();
    inputAdname = createRef();
    inputField = createRef();

    onRouteChangedUnListener = null;
    filterSetTimeoutHandler = 0;

    queryStringsObject = {
        name: "",
        date: "",
        adname: "",
        field: "",
    };

    AddRouteChangedListener = () => {
        this.onRouteChangedUnListener = this.props.history.listen(() => {
            this.UpdateFilterFormFromLocation();

            // Dispatch filter data
            Store.dispatch({
                type: Actions.DataManagementActions.SetFilteredData,
                payload: this.queryStringsObject,
            });
        });
    };

    /**
     * Update url query string when the filter changed
     *
     */
    onFilterChanged = () => {
        try {
            this.queryStringsObject.name = this.inputName.value;
            this.queryStringsObject.date = this.inputDate.value;
            this.queryStringsObject.adname = this.inputAdname.value;
            this.queryStringsObject.field = this.inputField.value;

            // Control typing delay time
            clearTimeout(this.filterSetTimeoutHandler);

            this.filterSetTimeoutHandler = setTimeout(() => {
                this.RedirectToNewQueryString();
            }, 1000);
        } catch (exception) {
            console.log(exception);
        }
    };

    /**
     * Parse the query string from location object and fill the filter form
     *
     */
    UpdateFilterFormFromLocation() {
        let queryString = decodeURI(this.props.history.location.search);
        let queryStirngArray = queryString.replace(/[?]/g, "").split("&");
        let queryStringKeys = Object.keys(this.queryStringsObject);
        let queryParams = [];

        queryStirngArray.forEach((item) => {
            let itemArray = item.split("=");
            queryParams[itemArray[0]] = itemArray[1];
        });

        queryStringKeys.forEach((key) => {
            if (queryParams.hasOwnProperty(key) === true) {
                this.queryStringsObject[key] = queryParams[key];
            } else {
                this.queryStringsObject[key] = "";
            }

            switch (key) {
                case "name":
                    this.inputName.value = this.queryStringsObject[key];
                    break;
                case "date":
                    this.inputDate.value = this.queryStringsObject[key];
                    break;
                case "adname":
                    this.inputAdname.value = this.queryStringsObject[key];
                    break;
                case "field":
                    this.inputField.value = this.queryStringsObject[key];
                    break;

                default:
                    break;
            }
        });
    }

    /**
     * Component did mout hook
     *
     */
    componentDidMount() {
        this.AddRouteChangedListener();
        this.UpdateFilterFormFromLocation();
        this.LoadData();
    }

    componentWillUnmount() {
        this.RemoveRouteChangedListener();
    }

    LoadData = () => {
        Store.dispatch({
            type: Actions.DataManagementActions.LoadWarehouseData,
            payload: {
                records: Data,
                filterParams: this.queryStringsObject,
            },
        });
    };

    RedirectToNewQueryString() {
        try {
            let queryString = "";
            const keys = Object.keys(this.queryStringsObject);

            keys.forEach((item, index) => {
                if (this.queryStringsObject[item] !== "") {
                    if (queryString === "") {
                        queryString += "?";
                    }

                    if (queryString === "?") {
                        queryString += `${item}=${this.queryStringsObject[item]}`;
                    } else {
                        queryString += `&${item}=${this.queryStringsObject[item]}`;
                    }
                }
            });

            // Redirect to new query string
            if (queryString !== this.props.history.location.search) {
                this.props.history.push({
                    pathname: "/",
                    search: queryString,
                    hash: "",
                });
            }
        } catch (exception) {
            console.log(exception);
        }
    }

    RemoveRouteChangedListener = () => {
        this.onRouteChangedUnListener();
    };

    render() {
        return (
            <div className="filter-form">
                <div className="form-control-container">
                    <input
                        className="form-control"
                        name="name"
                        onChange={() => this.onFilterChanged()}
                        onPaste={() => this.onFilterChanged()}
                        placeholder="نام تغییر دهنده"
                        ref={(el) => {
                            this.inputName = el;
                        }}
                        type="text"
                    />
                </div>
                <div className="form-control-container">
                    <input
                        className="form-control"
                        name="date"
                        onChange={() => this.onFilterChanged()}
                        onPaste={() => this.onFilterChanged()}
                        placeholder="تاریخ"
                        ref={(el) => {
                            this.inputDate = el;
                        }}
                        type="date"
                    />
                </div>
                <div className="form-control-container">
                    <input
                        className="form-control"
                        name="adname"
                        onChange={() => this.onFilterChanged()}
                        onPaste={() => this.onFilterChanged()}
                        placeholder="نام آگهی"
                        ref={(el) => {
                            this.inputAdname = el;
                        }}
                        type="text"
                    />
                </div>
                <div className="form-control-container">
                    <input
                        className="form-control"
                        name="field"
                        onChange={() => this.onFilterChanged()}
                        onPaste={() => this.onFilterChanged()}
                        placeholder="فیلد"
                        ref={(el) => {
                            this.inputField = el;
                        }}
                        type="text"
                    />
                </div>
            </div>
        );
    }
}

export default connect()(FilterForm);
