import App from "./App/App";
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom";
import Store from "./store/store";

import "./assets/css/font-awesome.css";
import "./index.css";

ReactDOM.render(
    <React.StrictMode>
        <Provider store={Store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById("root")
);
